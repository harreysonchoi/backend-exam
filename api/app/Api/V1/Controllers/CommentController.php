<?php

namespace App\Api\V1\Controllers;

use JWTAuth;
use App\Posts;
use App\Comments;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use App\Api\V1\Requests\CommentRequest;
use App\Http\Controllers\Controller;
use App\Api\V1\Transformers\CommentTransformer;
use App\Api\V1\Transformers\PostTraansformer;

class CommentController extends Controller
{
  use Helpers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $currentUser = JWTAuth::parseToken()->authenticate();

      return Posts::with('comments', 'replies')
        ->orderBy('created_at', 'DESC')
        ->paginate(25)
        ->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
      $currentUser = JWTAuth::parseToken()->authenticate();
      $comments = new Comments;

      $comments->body = $request->get('body');
      $comments->commentable_type = $request->get('commentable_type');
      $comments->commentable_id = $request->get('commentable_id');
      $comments->creator_id = $request->get('creator_id');
      $comments->parent_id = $request->get('parent_id');

      // Save post attach currrent user with it
      if($comments->save())
          return $this->response->item($comments, new CommentTransformer);
      else
          return $this->response->error('Unable to create user comments.', 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $currentUser = JWTAuth::parseToken()->authenticate();

      $comments = $currentUser->posts()->find($id)->comments();

      if(!$comments)
        return $this->response->error('The given data was invalid.', 500);

      return $this->response->item($comments, new CommentTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $currentUser = JWTAuth::parseToken()->authenticate();
      $comments = Comments::find($id);

      $comments->body = $request->get('body');
      $comments->commentable_type = $request->get('commentable_type');
      $comments->commentable_id = $request->get('commentable_id');
      $comments->creator_id = $request->get('creator_id');
      $comments->parent_id = $request->get('parent_id');

      // Save post attach currrent user with it
      if($comments->save())
          return $this->response->item($comments, new CommentTransformer);
      else
          return $this->response->error('Unable to create user comments.', 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($post_id, $comment_id)
    {
      $currentUser = JWTAuth::parseToken()->authenticate();

      $posts = $currentUser->posts()->find($post_id)->comments()->find($comment_id);

      if(!$posts)
        return $this->response->error('The given data was invalid.', 404);

      if($posts->delete())
          return $this->response->array(['status' => 'Record successfully deleted.'], 200);
      else
          return $this->response->error('The given data was invalid.', 500);
    }
}
