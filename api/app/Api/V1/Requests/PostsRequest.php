<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class PostsRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.posts.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
