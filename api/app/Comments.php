<?php

namespace App;

use App\Posts;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
  protected $guarded = ['id'];

  protected $fillable = ['body', 'commentable_type', 'commentable_id','parent_id','creator_id'];

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  public function posts()
  {
    return $this->belongsTo(Posts::class, 'commentable')->whereNull('parent_id');
  }
}
