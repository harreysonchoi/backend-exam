<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$apicall = app(Router::class);

$apicall->version('v1', function (Router $apicall) {
    $apicall->group(['prefix' => ''], function(Router $apicall) {
        $apicall->get('posts', 'App\\Api\\V1\\Controllers\\PostController@index');
        $apicall->get('posts/{post}', 'App\\Api\\V1\\Controllers\\PostController@show');
        $apicall->get('posts/{post}/comments', 'App\\Api\\V1\\Controllers\\CommentController@index');

        $apicall->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $apicall->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');
        $apicall->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $apicall->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
        $apicall->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $apicall->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $apicall->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');
    });

    $apicall->group(['middleware' => 'jwt.auth'], function ($apicall) {
      $apicall->post('posts', 'App\\Api\\V1\\Controllers\\PostController@store');
      $apicall->put('posts/{post}', 'App\\Api\\V1\\Controllers\\PostController@update');
      $apicall->delete('posts/{post}', 'App\\Api\\V1\\Controllers\\PostController@destroy');
      $apicall->post('posts/{post}/comments', 'App\\Api\\V1\\Controllers\\CommentController@store');
      $apicall->put('posts/{post}/comments/{comments}', 'App\\Api\\V1\\Controllers\\CommentController@update');
      $apicall->delete('posts/{post}/comments/{comments}', 'App\\Api\\V1\\Controllers\\CommentController@destroy');
    });


});
